{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "422cbf87-bcbb-409b-aa47-d74f353f3f4d",
   "metadata": {},
   "source": [
    "# Miscellaneous best practices"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a1ed3ca9-9c93-4546-9599-84b22c5ab7b0",
   "metadata": {},
   "source": [
    "## Logging\n",
    "\n",
    "A common, simple way of _debugging_ code during development as well as for\n",
    "keeping users updated about what's happening in the program right now is by\n",
    "adding `print()` statements throught the codebase.\n",
    "\n",
    "While this works, it is not very flexible, e.g., you probably _don't_ want the\n",
    "user to see _debugging_ messages when they just run the code in _production_.\n",
    "Also, it is more difficult to set up logging via `print()` statements to\n",
    "different output streams, like screen output, writing to files etc.\n",
    "\n",
    "Python comes with the `logging` module to make logging more convenient,\n",
    "flexible and effective, as it has multiple configuration options for\n",
    "formatting, configuring handlers/outputs and more. Moreover, it comes, by\n",
    "default, with several builtin _log levels_ that you can attach to each log\n",
    "message. These are the following (together with some loose conventions of how\n",
    "they are being used; ultimately, that is, of course, up to you), ordered in\n",
    "increasing severity:\n",
    "\n",
    "* `DEBUG`: messages not to be printed during normal operation\n",
    "* `INFO`: general messages for the user\n",
    "* `WARNING`: messages that the user should pay extra attention to, but that are\n",
    "   not considered errors\n",
    "* `ERROR`: messages that report on recoverable faulty/erroneous program\n",
    "   exectuion\n",
    "* `CRITICAL`: messages reporting on unrecoverable errors (the program exits\n",
    "   with an error status)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5fce4b4b-549c-4412-8143-762f635266ac",
   "metadata": {},
   "source": [
    "### A simple example\n",
    "\n",
    "It is very easy to use the builtin `logging` module. Simply import it,\n",
    "configure it, create a logger and then use it in your error messages. Here is\n",
    "an example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e4c18d51-c19e-4938-8dd5-88b46d177d89",
   "metadata": {},
   "outputs": [],
   "source": [
    "import logging\n",
    "\n",
    "# configure the \"root logger\", from which all logger instances inherit\n",
    "# this is OPTIONAL, you can also use the default options\n",
    "logging.basicConfig(\n",
    "    format='[%(asctime)s: %(levelname)s] %(message)s',\n",
    "    level=logging.INFO,\n",
    ")\n",
    "\n",
    "# set up a logger instance for the current module\n",
    "LOG = logging.getLogger(__name__)\n",
    "\n",
    "# use the logger in your code\n",
    "LOG.info(\"Script starting...\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d1cb4eff-3e3a-4337-9c4e-c9f58f7d1fcc",
   "metadata": {},
   "source": [
    "So what's happening here?\n",
    "\n",
    "1. First, we are **`import`ing** the [`logging`](https://docs.python.org/3/library/logging.html)\n",
    "   module as usual.\n",
    "   > As `logging` is a standard/builtin module, we do not need to install it.\n",
    "2. We then **configure** the _root logger_ with the\n",
    "   [`.basicConfig()`](https://docs.python.org/3/library/logging.html#logging.basicConfig)\n",
    "   method by passing it two (optional) parameters:\n",
    "\n",
    "   - `format`: defines the format of the log messages (see\n",
    "      [here](https://docs.python.org/3/library/logging.html#logrecord-attributes) for a list of attributes you can use)\n",
    "   - `level`: the minimum severity of log levels to log; here, we are only\n",
    "      logging `logging.INFO` and more severe messages, i.e., we are _not_\n",
    "      logging any debug messages  \n",
    "  \n",
    "   > Note that you can also use a configuration file together with the\n",
    "   > [`.fileConfig()`](https://docs.python.org/3/library/logging.config.html#logging.config.fileConfig)\n",
    "   > method for advanced configuration use cases.\n",
    "3. We are then creating a **logger instance**.\n",
    "   > In principle it is also possible\n",
    "   to skip this step and use the _root logger_ by using, e.g.,\n",
    "   `logging.debug(\"My debug message...\")`, but that is considered bad pratice.\n",
    "   The `.getLogger()` method takes as its required, positional argument the\n",
    "   name of the logger, i.e., a string. It is a convention to pass it `__name__`\n",
    "   in most cases, as this makes sure that in a program with multiple modules,\n",
    "   each module has its own logger instance. We now have our logger instance\n",
    "   stored in the variable `LOG`\n",
    "4. Finally, we use the logger to log a message via one of its convenience\n",
    "   methods.\n",
    "   > Next to `.info()` there are also corresponding methods for all other\n",
    "   > log levels, e.g., `.debug()`, `.error()`\n",
    "   etc. messages to log messages with the corresponding log level."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e9676522-59ef-494c-be3d-07035a1c3192",
   "metadata": {},
   "source": [
    "### Logging across multiple modules\n",
    "\n",
    "So do we have to set up logging like that in every module?\n",
    "\n",
    "There are [several\n",
    "patterns](https://stackoverflow.com/questions/15727420/using-logging-in-multiple-modules)\n",
    "to set up logging across multiple modules. Here's a simple one that should\n",
    "serve many scenarios, and it allows you to configure logging only once\n",
    "(for each entry point into your program):\n",
    "\n",
    "You configure the _root logger_ in your entry point (here module `main.py`):\n",
    "\n",
    "```python\n",
    "# main.py\n",
    "\n",
    "import logging\n",
    "\n",
    "from my_module import my_func\n",
    "\n",
    "\n",
    "def main():\n",
    "    LOG.info(\"Program started\")\n",
    "    my_func()\n",
    "    LOG.info(\"Program finished\")\n",
    "\n",
    "\n",
    "if __name__ == '__main__':\n",
    "    logging.basicConfig(\n",
    "        format='[%(asctime)s: %(levelname)s] %(message)s (module \"%(module)s\")',\n",
    "        level=logging.INFO,\n",
    "    )\n",
    "    LOG = logging.getLogger(__name__)\n",
    "    main()\n",
    "```\n",
    "\n",
    "Given that we have the _root logger_ configured already (and all other loggers\n",
    "inherit from that logger), in any other (non-entry point) module, we can now\n",
    "just do:\n",
    "\n",
    "```python\n",
    "# my_module.py\n",
    "\n",
    "import logging\n",
    "\n",
    "LOG = logging.getLogger(__name__)\n",
    "\n",
    "\n",
    "def my_func():\n",
    "    LOG.info(\"This is from a function from another module\")\n",
    "\n",
    "```\n",
    "\n",
    "You can copy the code from above to two files, `main.py` and `my_module.py`,\n",
    "respectively, and run the code from the command line with:\n",
    "\n",
    "```bash\n",
    "python main.py\n",
    "```\n",
    "\n",
    "You will see the following output:\n",
    "\n",
    "```console\n",
    "[2021-11-02 16:08:16,945: INFO] Program started (module \"main\")\n",
    "[2021-11-02 16:08:16,945: INFO] This is from a function from another module (module \"my_module\")\n",
    "[2021-11-02 16:08:16,946: INFO] Program finished (module \"main\")\n",
    "```\n",
    "\n",
    "So, how did this work?\n",
    "\n",
    "Given that we started the program from the command line, `__name__` is set to\n",
    "`__main__` and so the logger is configured and the `main()` function called.\n",
    "Here, there is first a log message to tell us that the program started, then\n",
    "code from a function in another module (that we imported) is executed (leading\n",
    "to another log message being written from there) and then finally we receive\n",
    "a message that the program concluded. Note that each call now includes the name\n",
    "of the module the message was logged in. This is because we included the\n",
    "`%(module)s` argument in the format string.\n",
    "\n",
    "But what happens if we import the module `my_module` from another program\n",
    "because we offer some library code that is useful in various programs? There\n",
    "won't be any call to `.basicConfig()` and thus the root logger and the module-\n",
    "level logger won't be configured in the same way! That's correct - and it is\n",
    "actually desirable. It is always the calling program/client that should\n",
    "configure logging. Therefore, put your logging configuration only in your\n",
    "entry point modules, and then just import and create a logger in all of your\n",
    "other modules with the pattern:\n",
    "\n",
    "```python\n",
    "import logging\n",
    "\n",
    "LOG = logging.getLogger(__name__)\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bfbbd01c-036b-4222-9871-12e11292e2fc",
   "metadata": {},
   "source": [
    "### Further reading\n",
    "\n",
    "- [Detailed How-To](https://docs.python.org/3/howto/logging.html)\n",
    "- [Official docs](https://docs.python.org/3/library/logging.html)\n",
    "- [Format string attributes](https://docs.python.org/3/library/logging.html#logrecord-attributes)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6d04ef3c-af76-4334-93e4-d87f7f267341",
   "metadata": {},
   "source": [
    "## Typing\n",
    "\n",
    "By original design, Python is a _dynamically typed_ language, meaning that a\n",
    "variable's type (e.g., `int` or `str`) can change over time. This also usually\n",
    "means (and including in Python) that a variable's initial type does not have\n",
    "to be declared before using the variable. Thus, as you know, you can just do\n",
    "something like that without problems:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "42bdb2e0-eb74-42c7-b578-7ab2ef092bdf",
   "metadata": {},
   "outputs": [],
   "source": [
    "x = 18\n",
    "print(f\"Value of x: {x}\")\n",
    "print(f\"Type of x: {type(x)}\")\n",
    "x = \"Now I'm a string\"\n",
    "print(f\"Value of x: {x}\")\n",
    "print(f\"Type of x: {type(x)}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e6b9d29c-5cf0-4f21-aec6-d04cbb579e42",
   "metadata": {},
   "source": [
    "This is in contrast to _statically typed_ languages, such as Java or C, where\n",
    "variable types have to be declared first. In C, e.g., this could look like\n",
    "this:\n",
    "\n",
    "```c\n",
    "int x;\n",
    "x = 18;\n",
    "int y = 20; /* you can also assign to the variable at the same time */\n",
    "z = 20; /* raises an error because the type of z hadn't been declared! */\n",
    "```\n",
    "\n",
    "In addition to being _dynamically typed_, Python is also a\n",
    "[_duck-typed_](https://en.wikipedia.org/wiki/Duck_typing) language, meaning\n",
    "that whether the current value of a given variable matches a required type is\n",
    "evaluated at run time with the \"duck test\":\n",
    "\n",
    "> _\"If it walks like a duck and it quacks like a duck, then it must be a duck\"_\n",
    "\n",
    "An example (taken from [Wikipedia](https://en.wikipedia.org/wiki/Duck_typing)):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d5ddc1a4-e7bf-46a4-b8bb-b355cb0c116e",
   "metadata": {},
   "outputs": [],
   "source": [
    "class Duck:\n",
    "    def swim(self):\n",
    "        print(\"Duck swimming\")\n",
    "\n",
    "    def fly(self):\n",
    "        print(\"Duck flying\")\n",
    "\n",
    "class Whale:\n",
    "    def swim(self):\n",
    "        print(\"Whale swimming\")\n",
    "\n",
    "for animal in [Duck(), Whale()]:\n",
    "    animal.swim()\n",
    "    animal.fly()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b7af394b-0ee5-4483-8f4a-cdb87ddf2d54",
   "metadata": {},
   "source": [
    "So if we say \"everything that can swim is a duck\", then a whale is a duck.\n",
    "Until we get into a situation where we need a duck also needs to fly, hence\n",
    "the error above.\n",
    "\n",
    "Both _dyamic_ and _static_ typing systems for programming languages have\n",
    "advantages and disadvantages. The advantages of _dynamic_ typing, especially\n",
    "when _duck-typed_ is that they are less tedious and more flexible. The\n",
    "disadvantages are that they are not tedious enough and too flexible.\n",
    "Duck-typing takes a load of your mind to such an extent that you are prone not\n",
    "to think about your code _enough_ and hence are more likely to introduce\n",
    "errors, especially in edge cases. And once a bug is introduced into your\n",
    "codebase, _dynamic typing_ will also make it a lot harder to spot it. Or write\n",
    "unit tests for your code, because it is difficult to foresee all of the\n",
    "different scenarios in which that code may be used and, e.g., what type of\n",
    "inputs your function may receive in these scenarios. In other words, _dynamic\n",
    "typing_ tends to be more dangerous, especially for more complex codebases!\n",
    "\n",
    "One other major disadvantage of _dynamic typing_ nowadays is that you cannot\n",
    "use the full power of modern, _smart_ editors, which are able to check your\n",
    "code for (potential) issues that may arise from _duck typing_. If the types are\n",
    "not known until the code is run, the editor cannot help you in spotting these!"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "77b8fb8c-ca03-4657-8118-35699ce966e5",
   "metadata": {},
   "source": [
    "### Type hints\n",
    "\n",
    "Up until Python 3.5, it was actually impossible to even declare types for\n",
    "variables. Due to the disadvantages of _dynamic typing systems_ mentioned in\n",
    "the previous section, an _optional_ \"type hinting\" system was introduced since.\n",
    "In Python 3.9 and above, this is now quite mature, and we strongly recommend\n",
    "you to make use of it for production code. But you may perhaps skip it for your\n",
    "unit tests - best of both worlds!\n",
    "\n",
    "So how does it work? It's quite simple!\n",
    "\n",
    "\n",
    "> Note that the typing system in Python and the `typing` module have been\n",
    "> undergoing a lot of changes since they were first introduced in Python 3.5.\n",
    "> We are referring here to how things are done in Python 3.9, where the system\n",
    "> is more mature and has stabilized to some degree. Note that if you need to\n",
    "> support older Python versions, you may need to do things slightly differently\n",
    "> (generally the older ways are still supported in the newer Python minor\n",
    "> releases).\n",
    "\n",
    "To declare the type of a version, you can do:\n",
    "\n",
    "```python\n",
    "x: int\n",
    "x = 18\n",
    "```\n",
    "\n",
    "Or you can declare the type and assign at the same type (more common):\n",
    "\n",
    "```python\n",
    "x: int = 18\n",
    "```\n",
    "\n",
    "However, note that typing _is not enforced_! Unliked in C (see above), your\n",
    "runtime won't complain at all if you do something like that:\n",
    "\n",
    "```python\n",
    "x: int\n",
    "x = \"But I'm a string!\"\n",
    "```\n",
    "\n",
    "Python remains a _dynamically typed_ language and the type hints are, well,\n",
    "just _hints_!\n",
    "\n",
    "So why should I bother with adding them, then?\n",
    "\n",
    "The answer is that you can use linters like `mypy` to check your code for\n",
    "typing issues. You will likely be able to configure your editor to use it and\n",
    "tell you in realtime if you run into potential issues. If you make use of\n",
    "type hinting, you should also include `mypy` in your CI. Just include a call\n",
    "`mypy name_of_your_package` and it will report any issues it finds. After\n",
    "coding for a bit, even after passing all your other linter tests, you might be\n",
    "amazed what issues `mypy` finds!\n",
    "\n",
    "Let's look at how to use type hints in functions and methods:\n",
    "\n",
    "```python\n",
    "def my_func(\n",
    "    a: list,\n",
    "    b: bool = False,\n",
    ") -> str:\n",
    "    # my code\n",
    "```\n",
    "\n",
    "Here we have defined a function that takes one required parameter `a` that is\n",
    "supposed to be of type `list`, as well as an optional (default value provided!)\n",
    "parameter `b` that is of type `bool`. The _return type_ is declared as `str`.\n",
    "We recommend you to use type hints at the very least for your functions so that\n",
    "your interfaces are well defined. It also helps you with writing docstrings, as\n",
    "you don't need to bother with adding variable types in them. Tools that are\n",
    "able to process properly formatted docstrings (e.g., Google-style docstrings)\n",
    "will detect the types from the hints in the function/methods signature. This is\n",
    "of course better, because a docstring is just text, it doesn't enforce\n",
    "anything, even if you use `mypy`. You can declare a parameter to require a\n",
    "certain type, but then the actual implementation uses another type. Docstrings\n",
    "tend to degrade more easily, and the real source of truth is always the code\n",
    "itself!\n",
    "\n",
    "Let's look at some more type hints:\n",
    "\n",
    "```python\n",
    "a: list[str]  # a list of strings\n",
    "b: tuple[str, int]  # a tuple with two items, the first a string, the second an integer\n",
    "c: dict  # a dictionary\n",
    "d: dict[str, int]  # a dictionary with the keys beings strings and the values being integers\n",
    "```\n",
    "\n",
    "If you want your variables to _optionally_ accept `None` (in addition to the\n",
    "declared type), you can use `Optional` from the `typing` module:\n",
    "\n",
    "```python\n",
    "from typing import Optional\n",
    "\n",
    "a: Optional[list[str]]  # here we accept a list of strings, or `None`\n",
    "```\n",
    "\n",
    "Two other useful features of the `typing` module are `Union` and `Any`. They\n",
    "allow you to specify more than one type:\n",
    "\n",
    "```python\n",
    "from typing import (Any, Union)\n",
    "\n",
    "a: Union[str, int]  # accepts a string OR an integer\n",
    "b: Any  # accepts any type\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5d5b4dbe-d114-4eff-b385-e58159125f36",
   "metadata": {},
   "source": [
    "### Further reading\n",
    "\n",
    "Of course there's a lot more to the new Python typing system, but you will\n",
    "probably be able to get quite far with the rather simple examples above. Once\n",
    "you run into situations where they won't be enough (or if you want to support\n",
    "Python version below 3.9), you can check the following resources:\n",
    "\n",
    "* [Official documentation](https://docs.python.org/3/library/typing.html)\n",
    "* [PEP 484](https://www.python.org/dev/peps/pep-0484/)\n",
    "* [Cheat sheet](https://mypy.readthedocs.io/en/latest/cheat_sheet_py3.html)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a82b1d85-e2f5-4881-80a1-81d801030635",
   "metadata": {},
   "source": [
    "# Homework (optional)\n",
    "\n",
    "1. Add some useful log messages to your code (info, debug, warning) etc.\n",
    "   Replace all `print()` statements.\n",
    "2. Add type hints to your code.\n",
    "3. Install `mypy`, set up your editor to use it, run it before committing new\n",
    "   code and add it to your CI configuration."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
<<<<<<< HEAD
}
=======
}
>>>>>>> 0e83768f31a0c245c20b65320189bfaf97df8c95
