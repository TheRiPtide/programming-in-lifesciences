import logging
import argparse
from transcript_sampler import TranscriptSampler as ts


parser = argparse.ArgumentParser(description='Sample from a gene transcript file.')
parser.add_argument('input_file', type=str, help='The file to sample from.')
parser.add_argument('output_file', type=str, help='The file to write to.')
parser.add_argument('number', type=int, help='The number of samples to take.')


def main():
    """Commandline interface for the TranscriptSampler class.

    Returns:
        None
    """
    args = parser.parse_args()

    avg_expressions = ts.read_avg_expression(args.input_file)

    sampled_expressions = ts.sample_transcripts(avg_expressions, args.number)

    ts.write_sample(args.output_file, sampled_expressions)

    return None


if __name__ == '__main__':
    logging.basicConfig(
        level=logging.INFO,
        format='%(asctime)s [%(levelname)s]: %(message)s (module "%(module)s")')
    main()
