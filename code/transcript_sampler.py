import numpy as np
import random


class TranscriptSampler:

    @staticmethod
    def read_avg_expression(file):
        """Reads file in format "gene_id nr_copies" and returns a dict of average expression per gene.

        Args:
            file (str): Filepath to the files from which data is read.

        Returns:
            dict: average expression in a dictionary in the form "gene": gene_avg_expression

        """
        all_expressions = {}
        avg_expression = {}

        with open(file, 'r') as f:

            for line in f:

                gene_id, nr_copies = line.split(' ')

                if gene_id in all_expressions.keys():

                    all_expressions[gene_id].append(int(nr_copies))

                else:

                    all_expressions[gene_id] = [int(nr_copies)]

        for gene_id, expressions in all_expressions.items():
            avg_expression[gene_id] = np.average(expressions)

        return avg_expression

    @staticmethod
    def sample_transcripts(avgs, number):
        """Returns a sample of transcripts from the supplied dict relative to their abundance.

        Args:
            avgs (dict): dict containing "gene_id": avg_expression key-value pairs
            number (int): number of samples to take

        Returns:
            dict: sampled dictionary

        """

        samples = {}
        genes = list(avgs.keys())
        weights = list(avgs.values())

        for i in range(number):

            gene_id = random.choices(genes, weights)[0]

            if gene_id in samples.keys():

                samples[gene_id] += 1

            else:

                samples[gene_id] = 1

        return samples

    @staticmethod
    def write_sample(file, sample):
        """Writes a transcript sample dict to a file.

        Args:
            file (str): file name to write to
            sample (dict): sample dict to write

        Returns:
             None

        """

        with open(file, 'w+') as file:

            for gene_id, expression in sample.items():

                file.write(f'{gene_id} {expression}')

        return None
