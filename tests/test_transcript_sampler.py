import pytest
from code.transcript_sampler import TranscriptSampler as ts


@pytest.mark.parametrize(
    "test_input,expected",
    [('./tests/resources/dummy_genes.txt', {"ADJKL": 2.5, "ASDFG": 3})]
)
def test_read_avg_expression(test_input, expected):
    assert (ts.read_avg_expression(test_input) == expected)


@pytest.mark.parametrize(
    "test_input,expected",
    [(({"ADJKL": 2.5, "ASDFG": 3}, 1), 1), (({"ADJKL": 2.5, "ASDFG": 3}, 4), 4)]
)
def test_read_avg_expression(test_input, expected):
    total = 0
    for v in ts.sample_transcripts(test_input[0], test_input[1]).values():
        total += v

    assert(total == expected)


@pytest.mark.parametrize(
    "test_input,expected",
    [('/dummypath', FileNotFoundError)]
)
def test_read_avg_expression_failing(test_input, expected):
    with pytest.raises(expected):
        ts.read_avg_expression(test_input)


@pytest.mark.parametrize(
    "test_input,expected",
    [(({"abcd": 1.2}, 1.1), TypeError)]
)
def test_sample_transcript_failing(test_input, expected):
    with pytest.raises(expected):
        ts.sample_transcripts(test_input)
