from setuptools import setup, find_packages
import code

setup(
    name='transcript_sampler',
    url='https://gitlab.com/TheRiPtide/programming-in-lifesciences',
    author='gregory zaugg',
    author_email='g.zaugg97@gmail.com',
    description='Samples genes from a gene expression file',
    license='MIT',
    version=code.__version__,
    packages=find_packages(),
    install_requires=['numpy~=1.21.2'],
    entry_points={
        'console_scripts': ['executable=code.cli:main'],
    }
)
